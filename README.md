# Ket PennyLane plugin

## Install 


```shell 
pip install pennylane-ket
```

or

```shell 
pip install git+https://gitlab.com/quantuloop/pennylane-ket.git
```

## Example

```python
import pennylane as qml
dev = qml.device('ket', wires=2)
```